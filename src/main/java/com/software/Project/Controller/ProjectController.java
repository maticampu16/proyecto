package com.software.Project.Controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ProjectController {

    @GetMapping("/sum/{num1}/{num2}")
    public String getNumSum(@PathVariable Integer num1, @PathVariable Integer num2) {
        return String.valueOf(num1 + num2);
    }
    @GetMapping("/hi")
    public String getGreeting() {
        return "hi";
    }

    public static Integer sum(Integer num1, Integer num2) {
        return num1 + num2;
    }
}
