package com.software.Project.Controller;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ProjectControllerTest {

    @Test
    public void sum() {
        Integer num1 = 5;
        Integer num2 = 3;

        assertEquals(num1 + num2 ,ProjectController.sum(num1,num2));
    }
}